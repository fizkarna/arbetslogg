const readline = require('readline');

const days = ['Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag', 'Söndag'];

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

const fs = require('fs');
const questionString = 'Vad gör du just nu?\n';
const divider = '\n------------------\n';

if (process.argv[2] === '-h' || process.argv[2] === '-help'){
	console.log(`
	Arbetslogg

	* För att stämpla in, skriv: IN
	* För att stämpla ut, skriv: UT
	* För att stämpla ut med friskvård, skriv: UTF
	* Avsluta alltid med Enter.
	* Skriv när du börjar med något nytt, för att enkelt kunna räkna ut timmarna som gått däremellan.
	`);
	rl.close();
}
else {
	runMain();
}

function runMain() {
	rl.question(questionString, save);
}

function save (answer){

	answer = answer.trim();
	const answerLowerCase = answer.toLowerCase();
	let isFriskvard = false;

	if(answerLowerCase == 'ut' ){
		answer = answer.toUpperCase() + divider;
	} else if(answerLowerCase == 'utf' ){
		answer = answer.toUpperCase().substring(0,2) + divider;
		printAnswer('Friskvård\n');
		isFriskvard = true;
	} else if (answerLowerCase == 'in'){
		answer = answer.toUpperCase() + ' ' + getDay();
	}
	else {
		answer = '\t' + answer;
	}

	answer += '\n';	

	printAnswer(answer, isFriskvard);

	rl.question(questionString, save);
}

function printAnswer(answer, isFriskvard) {

	const date = isFriskvard ? getNewDateString(1): getNewDateString();
	console.log(date + '\n ', answer);
	fs.appendFileSync('tider', date + ' ' + answer);
}

function getDay(){
	return days[new Date().getDay() - 1];
}

function getNewDateString(extraHours){

	extraHours = extraHours ? extraHours : 0;

	const d = new Date();
	const string = getLeadingZero(d.getDate()) + '/' + getLeadingZero(d.getMonth()+1) + ' ' + getLeadingZero(d.getHours() + extraHours) + ':' + getLeadingZero(d.getMinutes());
	return string;
}

function getLeadingZero(n){
	n = '' + n;
	if(n.length == 1){
		return '0' + n;
	}
	else{
		return n;
	}
}
