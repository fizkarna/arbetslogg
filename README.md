# README #

### What is this repository for? ###

* Ett enkelt javascript-program för att logga in- och utstämplingar med, samt andra sysslor däremellan.
* 1.1.0

### How do I get set up? ###

* För att köra lokalt, kör kommandot "node logg.js".
* För att få instruktioner, starta med flaggan "-help" eller "-h".